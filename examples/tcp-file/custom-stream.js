const stream = require( "stream" );
const util = require( "util" );


let timeout = util.promisify(setTimeout);

function BufferStream(source, options) {
    if (!Buffer.isBuffer(source) ) {
        throw( new Error( "Source must be a buffer." ) );
    }

    // Конструктор
    stream.Readable.call(this, options);
    // это просто буффер
    this._source = source;

    // Я отслеживаю, какая часть исходного буфера в настоящее время пушится
    // во внутренний поток буфера во время операций чтения.

    this._offset = 0;
    this._length = source.length;

    // Когда поток закончится, очищаю ссылки на память.
    this.on( "end", this._destroy );
}

util.inherits(BufferStream, stream.Readable);


// Я очищаю ссылки переменных после завершения потока.
BufferStream.prototype._destroy = function() {
    this._source = null;
    this._offset = null;
    this._length = null;

};


// Я читаю фрагменты из исходного буфера в буфере базового потока.
// ---------------------------------------------------------------
// Мы можем предположить, что значение размера всегда будет доступно, поскольку мы не
// изменяем параметры читаемого состояния при инициализации Readable stream.
BufferStream.prototype._read = function(size) {
    // highWaterMark size по умолчанию 16384  это 16 кб
    // Если мы не достигли конца исходного буфера, нажмите следующий фрагмент на внутренний буфер потока.
    if ( this._offset < this._length ) {
        timeout(2000).then(() => {
            this.push(this._source.slice(this._offset, (this._offset + size)));
            this._offset += size;
        });

    }

    // Если мы исчерпали весь исходный буфер, закройте читаемый поток.
    if ( this._offset >= this._length ) {
        // по окончанию чтения , в golang отсылается EOF(end of file), в node свой хак
        this.push(null);
    }
};

module.exports = BufferStream;